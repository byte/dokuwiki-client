import json
import yaml
import requests
import logging
from json import loads
from dw import add_text

DEBUG = False

if DEBUG:
    logging.basicConfig(level=logging.INFO)

TOKEN = None
BASE_URL = 'https://api.telegram.org/bot'

with open('config.yml', 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

TOKEN = cfg['bot']['token']
USERS = cfg['bot']['users']


def get_bot_info():
    req = requests.get(f'{TOKEN}/getMe')
    result = json.loads(req.content)
    if result['ok']:
        logging.info('Name: %s', result['result']['first_name'])
        logging.info('Username: %s', result['result']['username'])
    else:
        logging.error('Error')
        logging.info(req.content)
        logging.info(result)


def get_updates(update_id):
    req = requests.get(f'{BASE_URL}{TOKEN}/getUpdates?offset={update_id}')
    return json.loads(req.content)


def send_message(chat_id, text, message_id):
    data = {
        'chat_id': chat_id,
        'text': text,
        'reply_to_message_id': message_id
    }
    return requests.post(f'{BASE_URL}{TOKEN}/sendMessage', data)


def get_messages():
    update_id = 0
    chat_id = None
    messages = get_updates(update_id)['result']
    while messages:
        for message in messages:
            message_text = None
            update_id = message['update_id']

            if 'edited_message' in message:
                logging.info('Message was edited')
                msg_data = message['edited_message']
                chat_id = message['edited_message']['chat']['id']
            elif 'message' in message:
                msg_data = message['message']
                chat_id = message['message']['chat']['id']

            if 'text' in msg_data:
                logging.info('Text message')
                message_id = msg_data['message_id']
                message_text = msg_data['text']
                if chat_id:
                    send_message(chat_id, 'Сохранено.', message_id)
            elif 'photo' in msg_data:
                logging.info('Photo message')
                message_text = 'Photo'
                message_id = msg_data['message_id']
                if chat_id:
                    send_message(chat_id, 'Фотографии не принимаем.', message_id)
            else:
                logging.info('Unknown type')
                message_id = msg_data['message_id']
                if chat_id:
                    send_message(chat_id, 'Не знаю, что с этим делать.', message_id)

            logging.info('Message id: %s', message_id)
            logging.info('Message   : %s', message_text)
            logging.info('Chat id   : %s', chat_id)
        logging.info('Last update id: %s', update_id)
        update_id += 1
        messages = get_updates(update_id)['result']


def parse_wh_message(in_data):
    message = in_data.get('message') or in_data.get('edited_message')
    chat_id = message.get('chat', {}).get('id')
    message_id = message.get('message_id')
    message_text = message.get('text')
    msg_from = message.get('from', {}).get('username')
    if msg_from not in USERS:
        send_message(chat_id, 'В доступе отказано.', message_id)
        return None
    if not message_text and chat_id and message_id:
        send_message(chat_id, "Я не знаю, что с этим делать.", message_id)
        return None
    if message_text.startswith('/start'):
        send_message(chat_id, "Добро пожаловать. Тестовый Байтов бот.", message_id)
        return None
    elif message_text.startswith('/'):
        send_message(chat_id, "/start или просто текст, пожалуйста.", message_id)
        return None
    if add_text(message_text):
        send_message(chat_id, "Сохранено!", message_id)
    else:
        send_message(chat_id, "Что-то пошло не так :-(", message_id)


def parse_post_request(request, start_response):
    if request['REQUEST_METHOD'] == "POST":
        try:
            request_body_size = int(request.get('CONTENT_LENGTH', 0))
        except(KeyError, ValueError):
            request_body_size = 0
        request_body = request['wsgi.input'].read(request_body_size)
        in_data = loads(request_body)
        parse_wh_message(in_data)


def application(environ, start_response):
    parse_post_request(environ, start_response)
    start_response("200 OK", [("Content-Type", "text/plain")])
    yield b'Endpoint here.'
