import sys

import yaml
import dokuwiki
import logging

DEBUG = False

if DEBUG:
    logging.basicConfig(level=logging.INFO)

ALIAS = 'byteware'
HOST = None
USER = None
PASSWORD = None
LOGPAGE = None

with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)
try:
    HOST = cfg[ALIAS]['host']
    USER = cfg[ALIAS]['user']
    PASSWORD = cfg[ALIAS]['password']
    LOGPAGE = cfg[ALIAS]['logpage']
except:
    pass


def add_text(text):
    assert(text)
    wiki = dokuwiki.DokuWiki(HOST, USER, PASSWORD)
    logging.info("Text to add: %s", text)

    if wiki.pages.append(LOGPAGE, f'  *{text}\r\n'):
        logging.info('Page saved successfully')
        return True  # TODO: Fix
    else:
        return False  # TODO: Fix


if __name__ == "__main__":
    params = sys.argv
    if len(params) > 1:
        text = params[1]
        add_text(text)
