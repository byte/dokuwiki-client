# README #

Примитивный клиент для dokuwiki. Позволяет быстро постить заметки из консоли.
Тестировалось под python 3.6, Debian.

### Настройка ###

* Включить XML-RPC в Dokuwiki (Enable the XML-RPC interface in the “Authentication Settings” section)
* Установить параметр `remoteuser` в настройках Dokuwiki
* Склонировать репозиторий
* Установить зависимости из requirements.txt: `pip install -r reqirements.txt`
* Создать файл config.yml с настройками:
```
site_alias:
    host: http://doku.testsite.com
    user: username
    password: password_here
    logpage: page_name_here

bot:
    token: telegram_bot_token_here
    users: [Username1,Username2]

```
* Установить параметр `ALIAS` в dw.py в нужное значение (`site_alias`)
* `python3.6 dw.py "This is a test"`


### Бот ###

Бот для Telegram, постящий заметки в Dokuwiki.
Подключается через uwsgi.
Весь входящий текст просто сохраняется в страницу, указанную в параметре конфига `logpage`.

Конфиг Nginx:
```
server {
    listen 80;
    listen *:443 ssl;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_certificate	/path/to/certificate.pem;
    ssl_certificate_key /path/to/cert-key-file.pem;
    server_name servername.com
    access_log /var/log/nginx/bot.access.log;
    error_log /var/log/nginx/bot.error.log;

    location / {
        include uwsgi_params;
        access_log off;
        include uwsgi_params;
        uwsgi_param UWSGI_SCRIPT bot;
        uwsgi_param SCRIPT_NAME bot;
        uwsgi_param UWSGI_CHDIR /path/to/dokuwiki-client/;
        uwsgi_pass unix:///path/to/bot/bot.sock;
    }
}
```

Конфиг uwsgi (ini):
```
[uwsgi]
 virtualenv = /path/to/dokuwiki-client/
 chdir = /path/to/dokuwiki-client/
 file=/path/to/dokuwiki-client/bot.py
 socket = /path/to/bot/bot.sock
```
